#include "stdio.h"
#include "stdlib.h"
#define TAM 8
#define QNTPA /*quantidade de porta aviões */ 1
#define TAMPA /*Tamanho dos porta aviões*/ 4
#define QNTEN /*quantidade encouraçados*/ 2
#define TAMEN /*tamanho encouraçado*/ 3
#define QNTCR /*quantidade de cruzadores*/ 2
#define TAMCR /*tamanho cruzador*/ 2
#define QNTSUB /*quantidade de submarinos*/ 4
#define TAMSUB /*tamanho submarinos*/ 1



/*Comentários das funções abaixo da Main*/
void gera_tabuleiro(short int * ponteiro_para_no);
void imprime_tabuleiro(short int * ponteiro_para_no);
void monta_tabuleiro_jogador(short int * ponteiro_para_no);
void mudavalor(short int * ponteiro_para_no,int alvo);
short int recebe_coordenadas();
int verifica_embarcacao(short int primeira_coordenada,short int ultima_coordenada,short int tamanho_da_embarcacao);
int criaembarcacao(short int * ponteiro_para_no,int primeira_coordenada,int ultima_coordenada, int tamanho_da_embarcacao);


int main(int argc, char const *argv[]) {
  short int tabuleiro_jogador[TAM*TAM],tabuleiro_computador[TAM*TAM];/*tabuleiros do jogador e do computador, são vetores com 3 valores possíveis.
  0,1 e 2. A orientação no tabuleiro é: A1,A2,...,A8,B1,...,B8,...H8*/
  short int *quemJoga, *computador;/*ponteiros para os tabuleiros dos jogadores*/
  /*ponteiro do jogador recebe a posição A1 do tabuleiro*/
  quemJoga = &tabuleiro_jogador[0];
  computador = &tabuleiro_computador[0];

  gera_tabuleiro(quemJoga);
  gera_tabuleiro(computador);
  monta_tabuleiro_jogador(quemJoga);
  imprime_tabuleiro(quemJoga);

  return 0;
}

void gera_tabuleiro(short int * ponteiro_para_no) {
  int i;
  /*For passndo por todos os pontos do tabuleiro (TAM*TAM é a área)
  mudando o valor do tabuleiro para 0 "mar" que depois poderá ser alterado
  e em seguida direcionando o ponteiro para o próximo no do tabuleiro*/
  for(i = 0; i < TAM * TAM; i++){
    *ponteiro_para_no = 0;
    ponteiro_para_no++;
  }
}

void imprime_tabuleiro(short int * ponteiro_para_no){
  int i,j;
  char mar[7],navio[7]={'0','0','0','0','0','0'},afundado[7]={'X','X','X','X','X','X'};
  char * imprime;
  /*Criação da string mar*/
  for(i = 0; i < 6; i++){
    if(i%2 == 0){
      mar[i] = '~';
    }
    else{
      mar[i] = ' ';
    }
  }

  printf("      A      B      C      D      E      F      G      H\n\n");
  /*este for vai percorrer toda o vetor tabuleiro 3 vezes para que ele possa ser printado
  segundo o modelo de tabuleiro, Fileira para esse programa são 8 nós consecutivos
  ou seja uma matriz de 3x8, onde se repetem 3 vezes cada nó*/
  for(i = 0; i < TAM * TAM * 3; i++, ponteiro_para_no++){
    /*O valor que o ponteiro estiver apontando vai mudar o ponteiro de impressão para a string
    adequada*/
    if(*ponteiro_para_no == 0) imprime = mar;
    if(*ponteiro_para_no == 2) imprime = afundado;
    if(*ponteiro_para_no == 1) imprime = navio;

    switch(i%24){
    /*Quando estiver no primeiro nó da fileira vai printar o separador de fileiras, em seguida
    printa o printavel do valor do nó*/
    case 0:
      printf("\n   +------+------+------+------+------+------+------+------+\n   |%s|",imprime);
      break;
    case 7:
    /*quando estiver no ultimo no da primeira linha da fileira printa o valor do nó
    e corrige ponteiro para voltar para o primeiro nó*/
      printf("%s|",imprime);
      ponteiro_para_no -= 8;
      break;
    /*quando estiver no ultimo no da segunda linha da fileira printa o valor do nó
    e corrige ponteiro para voltar para o primeiro nó*/
    case 15:
      printf("%s|",imprime);
      ponteiro_para_no -= 8;
      break;
    /*Corrige quando estiver na linha do meio da fileira printa o valor da fileira antes de
    printar o imprime do nó*/
    case 8:
    printf("\n%d  |%s|",(i/24)+1,imprime);
      break;
    case 16:
      printf("\n   |%s|",imprime);
      break;
    default:
      printf("%s|",imprime);
    }
  }
  printf("\n   +------+------+------+------+------+------+------+------+\n");
}

/*Função recebe entrada pelo usuário pelo teclado, valida e retorna a posição dela no tabuleiro*/
short int recebe_coordenadas(){
  static int quantas_vezes_usada = 0;
  char entrada[3];
  char caracteres_validos[TAM*2]= {'A','a','B','b','C','c','D','d','E','e','F','f','G','g','H','h'};
  int  valor = 0,i;/*validador para entrada correta e contador*/
  int erro = 0; /* caso entrada seja errada muda mensagem pro usuário*/
  /*Looping apenas se encerra quando valor for validado para a letra e
  numero da coordenada*/
  int valor_linha,valor_coluna;
  while(valor != 2){
    valor = 0;/*reinicia valor*/
    /*evita repetições desnecessárias*/
    if(quantas_vezes_usada == 0 && erro == 0){
      printf("Certo comandante, diga agora as coordenadas!\n");
      printf("Deve falar primeiro a coordenada da coluna (de A à H) seguida de um número (1 à 8)\n");
    }
    else if (erro > 0){
      printf("Coordenada invalida, por favor me diga uma letra e um número\n");
    }
    else{
      printf("Diga a coordenada por favor.\n");
    }

    fgets(entrada, 3, stdin);
    getchar();
    /*validação da coluna da coordenada*/
    for(i = 0; i < TAM * 2; i++){
      if(entrada[0] == caracteres_validos[i]){
        valor++;
        break;
      }
      erro++;
    }
    valor_linha = entrada[1] - '1'; /*da a coordenada da linha de 0 à 7*/
    if(valor_linha < 8 && valor_linha >=0){
      valor++;
    }
    else{
      erro++;
    }
  }
  /*usa a tabella asciii para colocar o valor de a = 0 até h = 7*/
  valor_coluna = entrada[0] - 'a';
  if(valor_coluna < 0){
    valor_coluna += 32;
  }
  /*a posição na fita pode ser pensada como uma tabela de divisão onde a linha
  representa o quociente e a coluna o resto, logo a posição será dada pela sentença abaixo*/
  printf("%d %d %d\n\n\n",valor_linha, valor_coluna,(valor_linha*8)+valor_coluna);
  return ((valor_linha*8)+valor_coluna);
  quantas_vezes_usada++;
}

void mudavalor(short int * ponteiro_para_no,int alvo){
  ponteiro_para_no += alvo; /*aponta para alvo*/
  (*ponteiro_para_no)++; /*muda seu valor*/
}

int verifica_embarcacao(short int primeira_coordenada,short int ultima_coordenada,short int tamanho_da_embarcacao){
  /*utiliza esquema de quociente e resto, para com quociente ser a linha
  e o resto para ser a coluna. Assim verifica se a embarcação tem o tamanho
  correto em relação ao esperado.*/
  int quociente_primeira, quociente_ultima;
  int resto_primeira, resto_ultima,tamanho_da_entrada;
  /*Transforma os valores para as variaveis quociente e resto*/
  quociente_primeira = primeira_coordenada/TAM;
  resto_primeira = primeira_coordenada%TAM;
  quociente_ultima = ultima_coordenada/TAM;
  resto_ultima = ultima_coordenada%TAM;
  /*Caso estejam na mesma coluna*/
  if(resto_ultima == resto_primeira){
    tamanho_da_entrada = quociente_ultima - quociente_primeira + 1;
    if(tamanho_da_entrada < 0)tamanho_da_entrada *= -1;
    if(tamanho_da_entrada == tamanho_da_embarcacao){
      return 1;
    }
    else{
      printf("A embarcação mede %d e não %d, mude as coordenadas por favor",tamanho_da_entrada,tamanho_da_embarcacao);
      return 0;
    }
  }
  /*Caso estejam na mesma linha*/
  else if(quociente_ultima == quociente_primeira){
    tamanho_da_entrada = resto_ultima - resto_primeira + 1;
    if(tamanho_da_entrada < 0)tamanho_da_entrada *= -1;
    if(tamanho_da_entrada == tamanho_da_embarcacao){
      return 1;
    }
    else{
      printf("A embarcação mede %d e não %d, mude as coordenadas por favor",tamanho_da_embarcacao,tamanho_da_entrada);
      return 0;
    }
  }



}

int criaembarcacao(short int * ponteiro_para_no,int primeira_coordenada,int ultima_coordenada,int tamanho_da_embarcacao){
  int temporario, i, alvo;
  /*coloca o menor valor na primeira coordenada*/
  if(primeira_coordenada > ultima_coordenada){
    temporario = ultima_coordenada;
    ultima_coordenada = primeira_coordenada;
    primeira_coordenada = temporario;
  }
  /*coloca o alvo na primeira_coordenada*/
  alvo = primeira_coordenada;
  /*Caso a embarcação esteja na vertical*/
  if((ultima_coordenada - primeira_coordenada) >= 8){
    for(i = 0; i < tamanho_da_embarcacao; i++){
      mudavalor(ponteiro_para_no, alvo);/*muda valor no tabuleiro onde está o alvo*/
      alvo +=8;/*manda alvo para próximo ponto da embarcação*/
    }
  }
  /*Caso a embaracação esteja na horizontal*/
  else{
    for(i = 0; i < tamanho_da_embarcacao; i++){
      mudavalor(ponteiro_para_no, alvo); /*muda valor do tabuleiro onde está o alvo*/
      alvo++;/*muda alvo para próximo ponto da embarcação*/
    }
  }
}

void monta_tabuleiro_jogador(short int * ponteiro_para_no){
  int i,primeira_coordenada,ultima_coordenada;/*contador*/
  /*DIALOGO*/
  int verifica = 0;
  printf("Que bom que chegou comandante, estava ao seu aguardo.\n");
  printf("\n\nNão temos tempo, a esquadra inimiga se aproxima...\n");
  printf("Preciso que ordene como vamos colocar nossa esquadra.\n");
  getchar();
  printf("\e[2J\e[H");

  /*PORTA AVIÕES*/
  printf("Começamos pelo porta aviões.Vou fazer o telefonema para o capitão, um momento\n");
  printf("...\n");
  while(verifica != 1){
    primeira_coordenada = recebe_coordenadas();
    ultima_coordenada = recebe_coordenadas();
    verifica += verifica_embarcacao(primeira_coordenada,ultima_coordenada,TAMPA);
  }verifica = 0;
  criaembarcacao(ponteiro_para_no,primeira_coordenada,ultima_coordenada,TAMPA);


}
